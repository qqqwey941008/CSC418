There are 4 type of basic structrure in my design. 
Head shape.
Body shape.
Cube.
Foot shape with 5 faces.

Body and arms are transformed body shape.
Head and beak are transformed head shape.
Hands and legs are transformed cube.
Feet are just foot shape.
The default position of the penguin is facing -x axis with -z to the left and z to the right.

Basic rendering options are done with function glPolygonMode with correspounding parameter.
Other 2 materiale rendering are done by changing ambiant, diffuse, specular and shineness.

Light source is moving in circle of 10 units above xz plane with 10 units radius clock-wise.
It start at [10,10,0].

UpdateKeyframe is a simple function that record the time and position of each DOF into a list with index ID.
The stored keyframes is a demo that I try to change every single DOF so you can play the animation to see all DOF.
