CSC418 A1 Report
Ziheng Liang, g5liangz, 1000393059

For this assignment, I've built a penguin with 10 degree of freedom. 6 of them are rotation w.r.t each joint, 1 is mouth movement, 1 is wing scaling and 2 are translation moving. 

Throughout the assignment, I've been using two drawing method. Legs and feet are scaling of given square in the starter code. All the other part are drawn directly using location of each point. 

The whole drawing is divided into several part. Each of them represent a group that their location are related to others in the group. For example, left foot location is related to left leg.

Kinematic Graph

Eye - Head - Mouth
        |
      Body - Wing
    /      \
Left leg Right Leg
   |           |
Left foot Right foot


Base shape.
Legs and feet are based on square. Others are based on themselves.

Animation "I believe I can fly"

All my animation functions have limited domain. Some are 0-60 frame and some are 0-15 frame depends on their purpose.
There are two type of functions. One of them is increasing linear function and the other one is sin function.
The use of these functions are straight forward when you see the animation visually.

