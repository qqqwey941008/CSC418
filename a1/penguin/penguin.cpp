/***********************************************************
             CSC418/2504, Fall 2011
  
                 robot.cpp
                 
       Simple demo program using OpenGL and the glut/glui 
       libraries

  
    Instructions:
        Please read the assignment page to determine 
        exactly what needs to be implemented.  Then read 
        over this file and become acquainted with its 
        design.

        Add source code where it appears appropriate. In
        particular, see lines marked 'TODO'.

        You should not need to change the overall structure
        of the program. However it should be clear what
        your changes do, and you should use sufficient comments
        to explain your code.  While the point of the assignment
        is to draw and animate the character, you will
        also be marked based on your design.
        
        Some windows-specific code remains in the includes; 
        we are not maintaining windows build files this term, 
        but we're leaving that here in case you want to try building
        on windows on your own.

***********************************************************/

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <glui.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifndef _WIN32
#include <unistd.h>
#else
void usleep(unsigned int nanosec)
{
    Sleep(nanosec / 1000);
}
#endif


// *************** GLOBAL VARIABLES *************************


const float PI = 3.14159;

// --------------- USER INTERFACE VARIABLES -----------------

// Window settings
int windowID;               // Glut window ID (for display)
GLUI *glui;                 // Glui window (for controls)
int Win[2];                 // window (x,y) size


// ---------------- ANIMATION VARIABLES ---------------------

// Animation settings
int animate_mode = 0;       // 0 = no anim, 1 = animate
int animation_frame = 0;      // Specify current frame of animation

// Joint parameters
const float HEAD_MIN = -45.0f;
const float HEAD_MAX =  45.0f;
float head_rot = 0.0f;

const float MOUTH_MIN = 0.0;
const float MOUTH_MAX = 10.0;
float mouth_trans = 0.0;

const float WING_MIN = -45.0f;
const float WING_MAX = 45.0f;
float wing_rot = 0.0f;

const float LEFT_LEG_MIN = -45.0f;
const float LEFT_LEG_MAX =  45.0f;
float left_leg_rot = 0.0f;

const float RIGHT_LEG_MIN = -45.0f;
const float RIGHT_LEG_MAX =  45.0f;
float right_leg_rot = 0.0f;

const float LEFT_FOOT_MIN = -15.0f;
const float LEFT_FOOT_MAX =  15.0f;
float left_foot_rot = 0.0f;

const float RIGHT_FOOT_MIN = -15.0f;
const float RIGHT_FOOT_MAX =  15.0f;
float right_foot_rot = 0.0f;

const float WING_SCALE_MIN = 0.8f;
const float WING_SCALE_MAX = 1.2f;
float wing_scale = 0.0f;

const float UP_DOWN_MIN = -100.0;
const float UP_DOWN_MAX = 100.0;
float up_down_trans = -100.0;

const float LEFT_RIGHT_MIN = -100.0;
const float LEFT_RIGHT_MAX = 100.0;
float left_right_trans = 100.0;

float test = 0.0;

//////////////////////////////////////////////////////
// TODO: Add additional joint parameters here
//////////////////////////////////////////////////////



// ***********  FUNCTION HEADER DECLARATIONS ****************


// Initialization functions
void initGlut(char* winName);
void initGlui();
void initGl();


// Callbacks for handling events in glut
void myReshape(int w, int h);
void animate();
void display(void);

// Callback for handling events in glui
void GLUI_Control(int id);


// Functions to help draw the object
void drawSquare(float size, int mode);
void drawBody(float szie, int mode);
void drawWing(float size, int mode);
void drawLeg(float size, int mode);
void drawFoot(float size, int mode);
void drawHead(float size, int mode);
void drawMouth(float size, int mode);
void drawEye(float size);
void drawCircle(float size, int mode, float r, float g, float b);

// Return the current system clock (in seconds)
double getTime();


// ******************** FUNCTIONS ************************



// main() function
// Initializes the user interface (and any user variables)
// then hands over control to the event handler, which calls 
// display() whenever the GL window needs to be redrawn.
int main(int argc, char** argv)
{

    // Process program arguments
    if(argc != 3) {
        printf("Usage: demo [width] [height]\n");
        printf("Using 600x800 window by default...\n");
        Win[0] = 600;
        Win[1] = 800;
    } else {
        Win[0] = atoi(argv[1]);
        Win[1] = atoi(argv[2]);
    }


    // Initialize glut, glui, and opengl
    glutInit(&argc, argv);
    initGlut(argv[0]);
    initGlui();
    initGl();

    // Invoke the standard GLUT main event loop
    glutMainLoop();

    return 0;         // never reached
}


// Initialize glut and create a window with the specified caption 
void initGlut(char* winName)
{
    // Set video mode: double-buffered, color, depth-buffered
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

    // Create window
    glutInitWindowPosition (0, 0);
    glutInitWindowSize(Win[0],Win[1]);
    windowID = glutCreateWindow(winName);

    // Setup callback functions to handle events
    glutReshapeFunc(myReshape); // Call myReshape whenever window resized
    glutDisplayFunc(display);   // Call display whenever new frame needed 
}


// Quit button handler.  Called when the "quit" button is pressed.
void quitButton(int)
{
  exit(0);
}

// Animate button handler.  Called when the "animate" checkbox is pressed.
void animateButton(int)
{
  // synchronize variables that GLUT uses
  glui->sync_live();

  animation_frame = 0;
  if(animate_mode == 1) {
    // start animation
    GLUI_Master.set_glutIdleFunc(animate);
  } else {
    // stop animation
    GLUI_Master.set_glutIdleFunc(NULL);
  }
}

// Initialize GLUI and the user interface
void initGlui()
{
    GLUI_Master.set_glutIdleFunc(NULL);

    // Create GLUI window
    glui = GLUI_Master.create_glui("Glui Window", 0, Win[0]+10, 0);

    // Create a control to specify the rotation of the joint
    //head
    GLUI_Spinner *head_spinner
        = glui->add_spinner("Head", GLUI_SPINNER_FLOAT, &head_rot);
    head_spinner->set_speed(0.1);
    head_spinner->set_float_limits(HEAD_MIN, HEAD_MAX, GLUI_LIMIT_CLAMP);

    //right leg
    GLUI_Spinner *right_leg_spinner
        = glui->add_spinner("Right Leg", GLUI_SPINNER_FLOAT, &right_leg_rot);
    right_leg_spinner->set_speed(0.1);
    right_leg_spinner->set_float_limits(RIGHT_LEG_MIN, RIGHT_LEG_MAX, GLUI_LIMIT_CLAMP);
    
    //left leg
    GLUI_Spinner *left_leg_spinner
        = glui->add_spinner("Left Leg", GLUI_SPINNER_FLOAT, &left_leg_rot);
    left_leg_spinner->set_speed(0.1);
    left_leg_spinner->set_float_limits(LEFT_LEG_MIN, LEFT_LEG_MAX, GLUI_LIMIT_CLAMP);
    
    //right foot
    GLUI_Spinner *right_foot_spinner
        = glui->add_spinner("Right Foot", GLUI_SPINNER_FLOAT, &right_foot_rot);
    right_foot_spinner->set_speed(0.1);
    right_foot_spinner->set_float_limits(RIGHT_FOOT_MIN, RIGHT_FOOT_MAX, GLUI_LIMIT_CLAMP);
    
    //left foot
    GLUI_Spinner *left_foot_spinner
        = glui->add_spinner("Left Foot", GLUI_SPINNER_FLOAT, &left_foot_rot);
    left_foot_spinner->set_speed(0.1);
    left_foot_spinner->set_float_limits(LEFT_FOOT_MIN, LEFT_FOOT_MAX, GLUI_LIMIT_CLAMP);
    
    //wing rot
    GLUI_Spinner *wing_spinner
        = glui->add_spinner("Wing Rotation", GLUI_SPINNER_FLOAT, &wing_rot);
    wing_spinner->set_speed(0.1);
    wing_spinner->set_float_limits(WING_MIN, WING_MAX, GLUI_LIMIT_CLAMP);

    //mouth
    GLUI_Spinner *mouth_spinner
        = glui->add_spinner("Mouth", GLUI_SPINNER_FLOAT, &mouth_trans);
    mouth_spinner->set_speed(0.1);
    mouth_spinner->set_float_limits(MOUTH_MIN, MOUTH_MAX, GLUI_LIMIT_CLAMP);
    
    //wing trans
    GLUI_Spinner *wing_scale_spinner
        = glui->add_spinner("Wing Scaling", GLUI_SPINNER_FLOAT, &wing_scale);
    wing_scale_spinner->set_speed(0.1);
    wing_scale_spinner->set_float_limits(WING_SCALE_MIN, WING_SCALE_MAX, GLUI_LIMIT_CLAMP);

    //up down
    GLUI_Spinner *up_down_spinner
        = glui->add_spinner("Up and Down", GLUI_SPINNER_FLOAT, &up_down_trans);
    up_down_spinner->set_speed(0.1);
    up_down_spinner->set_float_limits(UP_DOWN_MIN, UP_DOWN_MAX, GLUI_LIMIT_CLAMP);

      //mouth
    GLUI_Spinner *left_right_spinner
        = glui->add_spinner("Left and Right", GLUI_SPINNER_FLOAT, &left_right_trans);
    left_right_spinner->set_speed(0.1);
    left_right_spinner->set_float_limits(LEFT_RIGHT_MIN, LEFT_RIGHT_MAX, GLUI_LIMIT_CLAMP);
    
    ///////////////////////////////////////////////////////////
    // TODO: 
    //   Add controls for additional joints here
    ///////////////////////////////////////////////////////////

    // Add button to specify animation mode 
    glui->add_separator();
    glui->add_checkbox("Animate", &animate_mode, 0, animateButton);

    // Add "Quit" button
    glui->add_separator();
    glui->add_button("Quit", 0, quitButton);

    // Set the main window to be the "active" window
    glui->set_main_gfx_window(windowID);
}


// Performs most of the OpenGL intialization
void initGl(void)
{
    // glClearColor (red, green, blue, alpha)
    // Ignore the meaning of the 'alpha' value for now
    glClearColor(0.7f,0.7f,0.9f,1.0f);
}




// Callback idle function for animating the scene
void animate()
{
    // Update geometry

    double head_rot_t = (animation_frame % 60) / 60.0;
    head_rot = head_rot_t * HEAD_MIN + (1 - head_rot_t) * HEAD_MAX;
    
    double left_leg_rot_t = (animation_frame % 60) / 60.0;
    left_leg_rot = left_leg_rot_t * LEFT_LEG_MAX;
    
    double right_leg_rot_t = (animation_frame % 60) / 60.0;
    right_leg_rot = right_leg_rot_t * RIGHT_LEG_MAX;

    double left_foot_rot_t = (animation_frame % 60) / 60.0;
    left_foot_rot = left_foot_rot_t * LEFT_FOOT_MAX;

    double right_foot_rot_t = (animation_frame % 60) / 60.0;
    right_foot_rot = right_foot_rot_t * RIGHT_FOOT_MAX;
    
    double wing_rot_t = sin(10*((animation_frame % 5) / 5.0)/PI);
    wing_rot = wing_rot_t * WING_MIN + (1 - wing_rot_t) * WING_MAX;
    
    double mouth_trans_t = (animation_frame % 60) / 60.0;
    mouth_trans = mouth_trans_t * MOUTH_MIN + (1 - mouth_trans_t) * MOUTH_MAX;
    
    //double wing_scale_t = (animation_frame % 60) / 60.0;
    //wing_scale = wing_scale_t * WING_SCALE_MIN + (1 - wing_scale_t) * WING_SCALE_MAX;
    
    double up_down_t = sin(10*((animation_frame % 60) / 60.0)/PI);
    up_down_trans = -((up_down_t-1.0)*(up_down_t-1.0)-0.5)*(UP_DOWN_MAX - UP_DOWN_MIN);
    
    double left_right_t = (animation_frame % 60) / 60.0;
    left_right_trans = left_right_t * LEFT_RIGHT_MIN + (1 - left_right_t) * LEFT_RIGHT_MAX;
    
    test = animation_frame;
    ///////////////////////////////////////////////////////////
    // TODO:
    //   Modify this function animate the character's joints
    //   Note: Nothing should be drawn in this function!  OpenGL drawing
    //   should only happen in the display() callback.
    ///////////////////////////////////////////////////////////

    // Update user interface
    glui->sync_live();

    // Tell glut window to update itself.  This will cause the display()
    // callback to be called, which renders the object (once you've written
    // the callback).
    glutSetWindow(windowID);
    glutPostRedisplay();

    // increment the frame number.
    animation_frame++;

    // Wait 50 ms between frames (20 frames per second)
    usleep(50000);
}


// Handles the window being resized by updating the viewport
// and projection matrices
void myReshape(int w, int h)
{
    // Setup projection matrix for new window
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-w/2, w/2, -h/2, h/2);

    // Update OpenGL viewport and internal variables
    glViewport(0,0, w,h);
    Win[0] = w;
    Win[1] = h;
}



// display callback
//
// This gets called by the event handler to draw
// the scene, so this is where you need to build
// your scene -- make your changes and additions here.
// All rendering happens in this function.  For Assignment 1,
// updates to geometry should happen in the "animate" function.
void display(void)
{
    // glClearColor (red, green, blue, alpha)
    // Ignore the meaning of the 'alpha' value for now
    glClearColor(0.7f,0.7f,0.9f,1.0f);

    // OK, now clear the screen with the background colour
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  

    // Setup the model-view transformation matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    ///////////////////////////////////////////////////////////
    // TODO:
    //   Modify this function draw the scene
    //   This should include function calls to pieces that
    //   apply the appropriate transformation matrice and
    //   render the individual body parts.
    ///////////////////////////////////////////////////////////


    // Push the current transformation matrix on the stack
    glPushMatrix();
        // Draw the 'body'
        glTranslatef(0.0 + left_right_trans, -100.0 + up_down_trans, 0.0);
        glScalef(0.2,0.2,0.0);
        glPushMatrix();
            drawBody(1.0, GL_POLYGON);
        glPopMatrix();
        
        // Draw the 'wing'
        glPushMatrix();
            glTranslatef(50.0, 190.0, 0.0);

            glTranslatef(50.0, 170.0, 0.0);
            glRotatef(wing_rot, 0.0, 0.0, 1.0);
            glScalef(wing_scale, wing_scale, 0.0);
            glTranslatef(-50.0, -170.0, 0.0);

            drawWing(1.0, GL_POLYGON);

            glTranslatef(50.0, 170.0, 0.0);
            drawCircle(10, GL_LINE_LOOP, 0.0, 0.0, 0.0);
            glTranslatef(-50.0, -170.0, 0.0);

        glPopMatrix();

        // Draw the 'leg&foot'
        // leg: 45 * 150
        // foot: 200 * 40

        //right
        glPushMatrix();
            glTranslatef(130.0, 0.0, 0.0);

            glTranslatef(0.0, 45.0, 0.0);
            glRotatef(right_leg_rot, 0.0, 0.0, 1.0);
            glTranslatef(0.0, -45.0, 0.0);

            drawLeg(45, GL_POLYGON);
            glTranslatef(-85.0, -55.0, 0.0);

            glTranslatef(85.0, 100.0, 0.0);
            drawCircle(10, GL_LINE_LOOP, 0.0, 0.0, 0.0);
            glTranslatef(-85.0, -100.0, 0.0);

            glTranslatef(85.0, 0.0, 0.0);
            glRotatef(right_foot_rot, 0.0, 0.0, 1.0);
            glTranslatef(-85.0, 0.0, 0.0);

            drawFoot(200, GL_POLYGON);
            
            glTranslatef(85.0, 0.0, 0.0);
            drawCircle(10, GL_LINE_LOOP, 0.0, 0.0, 0.0);
            glTranslatef(-85.0, 0.0, 0.0);

        glPopMatrix();

        //left
        glPushMatrix();
            glTranslatef(0.0, 45.0, 0.0);
            glRotatef(left_leg_rot, 0.0, 0.0, 1.0);
            glTranslatef(0.0, -45.0, 0.0);

            drawLeg(45, GL_POLYGON);
            glTranslatef(-85.0, -55.0, 0.0);

            glTranslatef(85.0, 100.0, 0.0);
            drawCircle(10, GL_LINE_LOOP, 0.0, 0.0, 0.0);
            glTranslatef(-85.0, -100.0, 0.0);

            glTranslatef(85.0, 0.0, 0.0);
            glRotatef(left_foot_rot, 0.0, 0.0, 1.0);
            glTranslatef(-85.0, 0.0, 0.0);

            drawFoot(200, GL_POLYGON);




            glTranslatef(85.0, 0.0, 0.0);
            drawCircle(10, GL_LINE_LOOP, 0.0, 0.0, 0.0);
            glTranslatef(-85.0, 0.0, 0.0);

        glPopMatrix();

        //head&mouth&eye
        glPushMatrix();
            glTranslatef(-20.0, 500.0, 0.0);

            glTranslatef(80.0, 15.0, 0.0);
            glRotatef(head_rot, 0.0, 0.0, 1.0);
            glTranslatef(-80.0, -15.0, 0.0);

            drawHead(1.0, GL_POLYGON);

            glTranslatef(80.0, 15.0, 0.0);
            drawCircle(10, GL_LINE_LOOP, 0.0, 0.0, 0.0);
            glTranslatef(-80.0, -15.0, 0.0);

            glTranslatef(-90.0, 20.0, 0.0);
            drawMouth(1.0, GL_POLYGON);

            glTranslatef(130.0, 60.0, 0.0);
            drawEye(1.0);
        glPopMatrix();

        // Move the arm to the joint hinge
        // glTranslatef(0.0, -BODY_LENGTH/2 + ARM_WIDTH, 0.0);

        // // Rotate along the hinge
        // glRotatef(joint_rot, 0.0, 0.0, 1.0);

        // // Scale the size of the arm
        // glScalef(ARM_WIDTH, ARM_LENGTH, 1.0);

        // // Move to center location of arm, under previous rotation
        // glTranslatef(0.0, -0.5, 0.0);

        // // Draw the square for the arm
        // glColor3f(1.0, 0.0, 0.0);
        // //drawSquare(1.0);

    // Retrieve the previous state of the transformation stack
    glPopMatrix();


    // Execute any GL functions that are in the queue just to be safe
    glFlush();

    // Now, show the frame buffer that we just drew into.
    // (this prevents flickering).
    glutSwapBuffers();
}


// Draw a square of the specified size, centered at the current location
void drawSquare(float width, int mode)
{
    // Draw the square
    glBegin(mode);
    glVertex2d(-width/2, -width/2);
    glVertex2d(width/2, -width/2);
    glVertex2d(width/2, width/2);
    glVertex2d(-width/2, width/2);
    glEnd();
}

void drawBody(float size, int mode){
    //130
    glScalef(size, size, 0.0);
    glColor3f(0.0, 0.0, 204/225.0);
    glBegin(mode);
        glVertex2d(0,0);
        glVertex2d(130, 0);
        glVertex2d(234, 130);
        glVertex2d(130, 4*130);
        glVertex2d(0, 4*130);
        glVertex2d(-0.8*130, 130);
    glEnd();
}

void drawWing(float size, int mode){
    //100
    glColor3f(1.0, 128.0/255, 0.0);
    glScalef(size, size, 0.0);
    glBegin(mode);
    glVertex2d(16.6,0);
    glVertex2d(83.3, 0);
    glVertex2d(100, 200);
    glVertex2d(0, 200);
    glEnd();
}

void drawLeg(float size, int mode){
    //45 * 150
    glColor3f(1.0, 1.0, 51.0/255);
    glPushMatrix();
        glScalef(size, size * 150.0/45.0, 0.0);
        drawSquare(1.0, mode);
    glPopMatrix();
}

void drawFoot(float size, int mode){
    //200 * 40
    glColor3f(1.0, 1.0, 51.0/255);
    glPushMatrix();
        glScalef(size, size * 40.0/200.0, 0.0);
        drawSquare(1.0, mode);
    glPopMatrix();
}

void drawHead(float size, int mode){
    // 0,0 160,0 140,105 60,130 20,105 
    glColor3f(1.0, 128.0/255, 0.0);
    glPushMatrix();
        glScalef(size, size, 0.0);
        glBegin(mode);
        glVertex2d(0.0, 0.0);
        glVertex2d(160.0, 0.0);
        glVertex2d(140.0, 105.0);
        glVertex2d(60.0, 130.0);
        glVertex2d(20.0, 105.0);
        glEnd();
    glPopMatrix();
}

void drawMouth(float size, int mode){
    //0,0 110,0 110,10, 0,10
    //0,20 110,20 110,60, 0,35
    glPushMatrix();
        glColor3f(102.0/255, 1.0, 178.0/255);
        glScalef(size, size, 0.0);
        glBegin(mode);
        glVertex2d(0.0, 20.0);
        glVertex2d(110.0, 20.0);
        glVertex2d(110.0, 60.0);
        glVertex2d(0.0, 35.0);
        glEnd();

        glTranslatef(0.0, mouth_trans, 0.0);
        glBegin(mode);
        glVertex2d(0.0, 0.0);
        glVertex2d(110.0, 0.0);
        glVertex2d(110.0, 10.0);
        glVertex2d(0.0, 10.0);
        glEnd();


    glPopMatrix();
}

void drawEye(float size){
    drawCircle(size * 10, GL_LINE_LOOP, 0.0, 0.0, 0.0);
    drawCircle(size * 10, GL_POLYGON, 1.0, 1.0, 1.0);
    glTranslatef(-2.0, 0, 0);
    drawCircle(size * 5, GL_POLYGON, 0.0, 0.0, 0.0);
}

void drawCircle(float size, int mode, float r, float g, float b){
    const int sides = 20;
    glPushMatrix();
        glColor3f(r,g,b);
        glScalef(size, size, 0);
        glBegin(mode);
        for(int i = 0; i < 360; i+= 360.0/sides){
            double next  = i * PI / 180;
            glVertex2d(cos(next), sin(next));
        }
        glEnd();
    glPopMatrix();
}