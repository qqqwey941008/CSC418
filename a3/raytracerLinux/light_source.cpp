/***********************************************************
     Starter code for Assignment 3

     This code was originally written by Jack Wang for
		    CSC418, SPRING 2005

		implements light_source.h

***********************************************************/

#include <cmath>
#include <algorithm>
#include "light_source.h"

void PointLight::shade( Ray3D& ray ) {
	// TODO: implement this function to fill in values for ray.col 
	// using phong shading.  Make sure your vectors are normalized, and
	// clamp colour values to 1.0.
	//
	// It is assumed at this point that the intersection information in ray 
	// is available.  So be sure that traverseScene() is called on the ray 
	// before this function.  

	Vector3D s = this->get_position() - ray.intersection.point;
	Vector3D m = 2 * (s.dot(ray.intersection.normal)) * ray.intersection.normal - s; 
	s.normalize();
	m.normalize();
	//1.scene signature
	// ray.col = ray.intersection.mat->diffuse;

	//2.diffuse and ambiant
	// ray.col = ray.intersection.mat->ambient * _col_ambient +
	// 		std::max(0.0, ray.intersection.normal.dot(s)) * 
	// 		ray.intersection.mat->diffuse * _col_diffuse;

	//3.phong model
	ray.col = ray.intersection.mat->ambient * _col_ambient + 
	std::max(0.0, ray.intersection.normal.dot(s)) * 
	ray.intersection.mat->diffuse * _col_diffuse +
	pow(std::max(0.0, -ray.dir.dot(m)), ray.intersection.mat->specular_exp) *
	ray.intersection.mat->specular * _col_specular;
	

	ray.col.clamp();
	return ;
}

