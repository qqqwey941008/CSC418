Part1
raytracer.cpp
There are two type of ray-casting implemented in this file. Currently the program use the one with anti-aliasing. The one without anti-aliasing is commented out.
Anti-aliasing method
Each pixel the program does 16 ray-casting method. The final color of the pixel is the average of result color in the pixel.
Ray casting is using formular in section 11.2 in the PDF

light_source.cpp
This part follows the formular in section 11.7 in the PDF. There are 3 types of rendering, each with only diffuse, diffuse + ambiant or diffuse + ambiant + specular.

scene_object.cpp
This part follows the formulars in section 11.3 in the PDF. 