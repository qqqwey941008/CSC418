/***********************************************************
     Starter code for Assignment 3

     This code was originally written by Jack Wang for
		    CSC418, SPRING 2005

		implements scene_object.h

***********************************************************/

#include <cmath>
#include <iostream>
#include "scene_object.h"

bool UnitSquare::intersect( Ray3D& ray, const Matrix4x4& worldToModel,
		const Matrix4x4& modelToWorld ) {
	// TODO: implement intersection code for UnitSquare, which is
	// defined on the xy-plane, with vertices (0.5, 0.5, 0), 
	// (-0.5, 0.5, 0), (-0.5, -0.5, 0), (0.5, -0.5, 0), and normal
	// (0, 0, 1).
	//
	// Your goal here is to fill ray.intersection with correct values
	// should an intersection occur.  This includes intersection.point, 
	// intersection.normal, intersection.none, intersection.t_value.   
	//
	// HINT: Remember to first transform the ray into object space  
	// to simplify the intersection test.

	Point3D ray_origin = worldToModel * ray.origin;
	Vector3D ray_dir = worldToModel * ray.dir;
	Point3D p_1 = Point3D(0.0, 0.0, 0.0);
	Vector3D n = Vector3D(0.0, 0.0, 1.0);
	double lamda = (p_1 - ray_origin).dot(n) / (ray_dir.dot(n));
	Point3D point_on_plane = ray_origin + lamda * ray_dir;

	if(lamda<0){
		return false;
	}

	if(point_on_plane[0] > 0.5 ||
		point_on_plane[0] < -0.5 ||
		point_on_plane[1] > 0.5 ||
		point_on_plane[1] < -0.5){
		return false;
	}
	else{
		if(!ray.intersection.none){
			return false;
		}
		ray.intersection.point = modelToWorld * point_on_plane;
		ray.intersection.normal = worldToModel.transpose() * n;
		ray.intersection.normal.normalize();
		ray.intersection.t_value = lamda;

		ray.intersection.none = false;
	}
	return true;
}

bool UnitSphere::intersect( Ray3D& ray, const Matrix4x4& worldToModel,
		const Matrix4x4& modelToWorld ) {
	// TODO: implement intersection code for UnitSphere, which is centred 
	// on the origin.  
	//
	// Your goal here is to fill ray.intersection with correct values
	// should an intersection occur.  This includes intersection.point, 
	// intersection.normal, intersection.none, intersection.t_value.   
	//
	// HINT: Remember to first transform the ray into object space  
	// to simplify the intersection test.
	
	Point3D ray_origin = worldToModel * ray.origin;
	Vector3D ray_dir = worldToModel * ray.dir;

	Point3D origin = Point3D(0.0, 0.0, 0.0);

	double a = ray_dir.dot(ray_dir); // A = d /cdot d
	double b = (ray_origin - origin).dot(ray_dir); // B = (a - c) /cdot d
	double c = (ray_origin - origin).dot((ray_origin - origin)) - 1; //C = (a - c) /cdot (a - c) - 1
	double d = b * b - a * c; //D = B^2 - AC

	if(d < 0){	
		return false;
	}
	else{
		double lamda_1 = -1 * (b / a) + sqrt(d) / a;
		double lamda_2 = -1 * (b / a) - sqrt(d) / a;
		double true_lamda = lamda_1;
		if(lamda_1 < 0 && lamda_2 < 0){ // both not visible
			return false;
		}
		else if(lamda_1 < 0 && lamda_2 > 0){ //p(2) visible
			true_lamda = lamda_2;
		}
		else if(lamda_1 > 0 && lamda_2 < 0){ //p(1) visible
			true_lamda = lamda_1;
		}
		else if(lamda_1 > lamda_2){ //p(2) closer
			true_lamda = lamda_2;
		}
		if(!ray.intersection.none){
			return false;
		}

		ray.intersection.point = modelToWorld * (ray_origin + true_lamda * ray_dir);
		ray.intersection.t_value = true_lamda;
		ray.intersection.normal = worldToModel.transpose() * ((ray_origin + true_lamda * ray_dir) - origin);
		ray.intersection.normal.normalize();
		ray.intersection.none = false;
	}
	return true;
}

