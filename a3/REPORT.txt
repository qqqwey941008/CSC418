Ziheng Liang, Kenan Deng
g5liangz, g4dengke

Part1
raytracer.cpp
There are two type of ray-casting implemented in this file. Currently the program use the one with anti-aliasing. The one without anti-aliasing is commented out.
Anti-aliasing method
Each pixel the program does 16 ray-casting method. The final color of the pixel is the average of result color in the pixel.
Ray casting is using formular in section 11.2 in the PDF

light_source.cpp
This part follows the formular in section 11.7 in the PDF. There are 3 types of rendering, each with only diffuse, diffuse + ambiant or diffuse + ambiant + specular.

scene_object.cpp
This part follows the formulars in section 11.3 in the PDF. 


Part2  Animation/Interactive game using opengl
Introduction:
    Our game is based on assignment2. We reuse the penguin model to decrease the amount of workload. We combined the animation with interactive game and made a game with some animations.
    We animate the scene using key-frames, user interaction. We use a cubic interpolation model made by ourselves for smoothness. And we use user interaction to control the animation.
About the game:
    Use radio button on the panel to change difficulty.
    Use w,a,s,d to control the penguin.
    Dodge the incoming blocks. The portal is an indicator of an incoming block.
    Use space to restart the game if you died.
    Hold left/right mouse button and drag to adjust the camera position.
Keyframe:
    We use cubic interpolation to produce the value between the keyframes. The cubic interpolation class in cubicInterpolation.h is a class handling storing keyframe values and handling matrix solving. Once added 4 keyframes points (or 3 keyframe points with one derivative). Call cubic->solve() will solve the matrix and update the parameters' values. 
    Each animation is stored in separate file. 
Animation:
    We design 4 kinds of animation for the penguin, move forward, backward, left and right. They are short animations that use different numbers of keyframes to produce smooth animations. For moving left and right, we use 3 keyframes and 1 tangent to produce the cubic interpolation. For moving backward, we use 4 keyframes. For moving forward, we use 5 keyframes. Our cubic interpolation function will handle the corner case if there's not enough keyframes(like in between keyframe 0 and keyframe 1). Block and the portal are animated using linear interpolation.
User interaction:
    We handle the input from both keyboard and mouse. For keyboard, we handle w,a,s,d and space for controlling the game and animation. Press the any of the key that control direcation will trigger the animation to begin. And user need to adjust the position of the penguin in order to dodge the incoming blocks. For the mouse, we handle the dragging in horizontal direction. We make the camera circulate the penguin and fix on the penguin according to the penguin's position and the mouse's change of position in x.
Game mechanics:
    The game mechanics is simple. Dodge the incoming blocks. We handle the collision in the simple way: we use simple distance calculation to calculate the distance between the blocks' bounding box and the penguin's bounding box. Blocks are spawned in all 4 directions and their speed, position are random. Once they reach the edge of the platform, they will be redeployed. If penguin collides with the block, the game and animation will stop until user press the space bar.
Code structure:
In main.cpp
    initDS() will initialize the data structure. 

    init_animation() will initialize the four animation keyframes. It will read four files "w.txt","a.txt","s.txt","d.txt" where the keyframes are stored.
    
    getAnimatonInterpolatedJointDOFS() is the core function of our cubic interpolation, it will create cubicinterpolation object for each DOF of the penguin and do cubic interpolation and return the vector given the time.

    display() is the core function of displaying the objects in the scene.

    animate() is the core function of animating the scene.

    draw[Something]() are set of drawing function to draw the scene.
In enemy.cpp
    The enemy class is for storing the blocks information and some function for helping deploying the blocks.
In cubicInterpolation.h
    The CubicInterpolation is for cubic interpolation. This is in order to solve the 4 x 4 matrix given 4 time - value pair.

We reuse the part of assignment2 code. The orinal code of assignment2 (without the penguin and the animation) is written by Mike Pratscher.

