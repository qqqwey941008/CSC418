//
//  cubicInterpolation.hpp
//  csc411a2
//
//  Created by Kenan Deng on 2016-04-07.
//  Copyright © 2016 Kenan Deng. All rights reserved.
//

#ifndef cubicInterpolation_hpp
#define cubicInterpolation_hpp

#include <stdio.h>
#include <cmath>
#endif /* cubicInterpolation_hpp */

class CubicInterpolation{
    float W[4][4];
    float b[4];
    float a[4];
public:
    void addEquation(float t, float b, bool isderivative, int index){
        if (!isderivative){
            for (int i=0; i<4;i++){
                W[index][i] = powf(t,i);
            }
        }
        else{
            W[index][0] = 0;
            for(int i =0;i<3;i++){
                W[index][i+1] = (i+1) * powf(t,i);
            }
        }
        this->b[index] = b;
    }
    
    void solve(){
        float divide = 0;
        //Simple Gaussian Elminatino:
        for (int i = 0 ;i< 4;i++){
            for (int j = i+1;j<4 ;j++){
                if (W[i][i]!= 0){
                    divide = float(W[j][i])/W[i][i];
                }
                else{
                    for(int m = i+1 ;m<4;m++){
                        if (W[m][i]!= 0){
                            swap(m, i);
                        }
                    }
                    divide = W[j][i]/W[i][i];
                }
                for(int k=i;k<4;k++){
                    W[j][k] -= W[i][k] * divide;
                }
                b[j] -= b[i] * divide;
            }
        }
        //Back propagation:
        float temp = 0;
        for (int i = 3;i>=0;i--){
            temp = b[i];
            for (int j = i+1;j<4;j++){
                temp -= W[i][j] * a[j];
            }
            a[i] = temp/W[i][i];
        }
        
    }
    float calculate(float t){
        return a[0] + t * (a[1] + t *(a[2]+ a[3] * t));
    }
    void printM(){
        for (int i = 0;i<4;i++){
            for (int j = 0;j<4;j++){
                printf("%f ",W[i][j]);
            }
            printf("\n");
        }
        printf("\n");
        printf("a0:%f, a1:%f, a2:%f, a3:%f\n",a[0],a[1],a[2],a[3]);
    }
private:
    //Expensive swap. (Although not usual)
    void swap(int des, int src){
        float temp;
        for(int i = 0;i<4;i++){
            temp = W[src][i];
            W[src][i] = W[des][i];
            W[des][i] = temp;
        }
    }
};