/***********************************************************
             CSC418, Winter 2016
 
                 penguin.cpp
                 author: Mike Pratscher
                 based on code by: Eron Steger, J. Radulovich

		Main source file for assignment 2
		Uses OpenGL, GLUT and GLUI libraries
  
    Instructions:
        Please read the assignment page to determine 
        exactly what needs to be implemented.  Then read 
        over this file and become acquainted with its 
        design. In particular, see lines marked 'README'.
		
		Be sure to also look over keyframe.h and vector.h.
		While no changes are necessary to these files, looking
		them over will allow you to better understand their
		functionality and capabilites.

        Add source code where it appears appropriate. In
        particular, see lines marked 'TODO'.

        You should not need to change the overall structure
        of the program. However it should be clear what
        your changes do, and you should use sufficient comments
        to explain your code.  While the point of the assignment
        is to draw and animate the character, you will
        also be marked based on your design.

***********************************************************/

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <glui.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "keyframe.h"
#include "timer.h"
#include "vector.h"
#include "enemy.h"
#include "cubicInterpolation.h"


// *************** GLOBAL VARIABLES *************************


const float PI = 3.14159;

const float SPINNER_SPEED = 0.1;

// --------------- USER INTERFACE VARIABLES -----------------

// Window settings
int windowID;				// Glut window ID (for display)
int Win[2];					// window (x,y) size

GLUI* glui_joints;			// Glui window with joint controls
GLUI* glui_render;			// Glui window for render style

char msg[256];				// String used for status message
GLUI_StaticText* status;	// Status message ("Status: <msg>")


// ---------------- ANIMATION VARIABLES ---------------------

// Camera settings
bool updateCamZPos = false;
int  lastX = 0;
int  lastY = 0;
const float ZOOM_SCALE = 0.01;

GLdouble camXPos =  10;
GLdouble camYPos =  10;
GLdouble camZPos = 15;
GLdouble camRotate = 0;

const GLdouble CAMERA_FOVY = 60;
const GLdouble NEAR_CLIP   = 0.1;
const GLdouble FAR_CLIP    = 1000.0;

// Render settings
enum { WIREFRAME, SOLID, OUTLINED ,METALLIC,MATTE};	// README: the different render styles
int renderStyle = OUTLINED;			// README: the selected render style
float light_pos = 0;
// Animation settings
int animate_mode = 1;			// 0 = no anim, 1 = animate


Keyframe* keyframes;			// list of keyframes
Keyframe* Wkeyframes;	//W 1
Keyframe* Skeyframes;	//A 2
Keyframe* Akeyframes;	//S 3
Keyframe* Dkeyframes;	//D 4

int platform_length = 60;
int steplength = 4;
int platform_buffer = 3;
int maxValidWKeyframe = 0;
int maxValidAKeyframe = 0;
int maxValidSKeyframe = 0;
int maxValidDKeyframe = 0;
int curAnimationMode = 0;
int keypress = 0;
Enemy * enemies;
int maxEnemy = 200;
float enemy_rotate = 0;
int curEnemy = 10;
int maxValidKeyframe   = 0;		// index of max VALID keyframe (in keyframe list)
const int KEYFRAME_MIN = 0;
const int KEYFRAME_MAX = 32;	// README: specifies the max number of keyframes

// Frame settings
char filenameF[128];			// storage for frame filename

int frameNumber = 0;			// current frame being dumped
int frameToFile = 0;			// flag for dumping frames to file


int difficulty = 0;
int prev_difficulty = 0;
// Time settings
Timer* animationTimer;
Timer* frameRateTimer;

const float SEC_PER_FRAME = 1.0 / 60;

// Joint settings

// README: This is the key data structure for
// updating keyframes in the keyframe list and
// for driving the animation.
//   i) When updating a keyframe, use the values
//      in this data structure to update the
//      appropriate keyframe in the keyframe list.
//  ii) When calculating the interpolated pose,
//      the resulting pose vector is placed into
//      this data structure. (This code is already
//      in place - see the animate() function)
// iii) When drawing the scene, use the values in
//      this data structure (which are set in the
//      animate() function as described above) to
//      specify the appropriate transformations.
Keyframe* joint_ui_data;



// ***********  FUNCTION HEADER DECLARATIONS ****************


// Initialization functions
void initDS();
void initGlut(int argc, char** argv);
void initGlui();
void initGl();
void init_animation();
//
int updatePenguinPos(float oldvalue, float increment, int type);
// Callbacks for handling events in glut
void reshape(int w, int h);
void animate();
void display(void);
void mouse(int button, int state, int x, int y);
void motion(int x, int y);
void keyboard(unsigned char Key, int x, int y);

// Functions to help draw the object
Vector getInterpolatedJointDOFS(float time);
void drawCube();
void drawBody(int color);
void drawHead(int color);
void drawWings();
void drawFeet();
void drawMain();
void drawOutline();
void drawCircle(float r,int n, float x,float y, int direction);
// Image functions
float timenormal(float time, float starttime, float endtime);

// ******************** FUNCTIONS ************************



// main() function
// Initializes the user interface (and any user variables)
// then hands over control to the event handler, which calls 
// display() whenever the GL window needs to be redrawn.
int main(int argc, char** argv)
{

    // Process program arguments
    if(argc != 3) {
        printf("Usage: demo [width] [height]\n");
        printf("Using 1024x768 window by default...\n");
        Win[0] = 1024;
        Win[1] = 768;
    } else {
        Win[0] = atoi(argv[1]);
        Win[1] = atoi(argv[2]);
    }

    // Initialize data structs, glut, glui, and opengl
	initDS();
    initGlut(argc, argv);
    initGlui();
    initGl();

    // Invoke the standard GLUT main event loop
    glutMainLoop();

    return 0;         // never reached
}

float timenormal(float time, float starttime, float endtime){
    return (time-starttime)/(endtime-starttime);
}


// Create / initialize global data structures
void initDS()
{
	keyframes = new Keyframe[KEYFRAME_MAX];
	for( int i = 0; i < KEYFRAME_MAX; i++ )
		keyframes[i].setID(i);
    
    Wkeyframes = new Keyframe[KEYFRAME_MAX];
    for( int i = 0; i < KEYFRAME_MAX; i++ )
        Wkeyframes[i].setID(i);
    Akeyframes = new Keyframe[KEYFRAME_MAX];
    for( int i = 0; i < KEYFRAME_MAX; i++ )
        Akeyframes[i].setID(i);
    Skeyframes = new Keyframe[KEYFRAME_MAX];
    for( int i = 0; i < KEYFRAME_MAX; i++ )
        Skeyframes[i].setID(i);
    Dkeyframes = new Keyframe[KEYFRAME_MAX];
    for( int i = 0; i < KEYFRAME_MAX; i++ )
        Dkeyframes[i].setID(i);
    
    enemies = new Enemy[maxEnemy];
    float destx, desty;
    srand(time(NULL));
    for (int i =0; i< curEnemy;i++){
        enemies[i].setId(i);
//        enemies[i].setX(6 * cos(rand()%360 * PI/180 ));
//        enemies[i].setY(6 * sin(rand()%360 * PI/180 ));
//        enemies[i].setX(platform_length);
//        enemies[i].setY(rand()%platform_length-platform_length/2.0);
//        enemies[i].setSpeed();
        enemies[i].deploy(platform_length);
    }
    init_animation();
	animationTimer = new Timer();
	frameRateTimer = new Timer();
	joint_ui_data  = new Keyframe();
    
}

void init_animation(){
    //W
    FILE* file = fopen("w.txt", "r");
    if( file == NULL )
    {
        return;
    }
    fscanf(file, "%d", &maxValidWKeyframe);
    for( int i = 0; i <= maxValidWKeyframe; i++ )
    {
        fscanf(file, "%d", Wkeyframes[i].getIDPtr());
        fscanf(file, "%f", Wkeyframes[i].getTimePtr());
        
        for( int j = 0; j < Keyframe::NUM_JOINT_ENUM; j++ )
            fscanf(file, "%f", Wkeyframes[i].getDOFPtr(j));
    }
    fclose(file);
    //S
    file = fopen("s.txt", "r");
    if( file == NULL )
    {
        return;
    }
    fscanf(file, "%d", &maxValidSKeyframe);
    for( int i = 0; i <= maxValidSKeyframe; i++ )
    {
        fscanf(file, "%d", Skeyframes[i].getIDPtr());
        fscanf(file, "%f", Skeyframes[i].getTimePtr());
        
        for( int j = 0; j < Keyframe::NUM_JOINT_ENUM; j++ )
            fscanf(file, "%f", Skeyframes[i].getDOFPtr(j));
    }
    fclose(file);
    //A
    file = fopen("a.txt", "r");
    if( file == NULL )
    {
        return;
    }
    fscanf(file, "%d", &maxValidAKeyframe);
    for( int i = 0; i <= maxValidAKeyframe; i++ )
    {
        fscanf(file, "%d", Akeyframes[i].getIDPtr());
        fscanf(file, "%f", Akeyframes[i].getTimePtr());
        
        for( int j = 0; j < Keyframe::NUM_JOINT_ENUM; j++ )
            fscanf(file, "%f", Akeyframes[i].getDOFPtr(j));
    }
    fclose(file);
    //D
    file = fopen("d.txt", "r");
    if( file == NULL )
    {
        return;
    }
    fscanf(file, "%d", &maxValidDKeyframe);
    for( int i = 0; i <= maxValidDKeyframe; i++ )
    {
        fscanf(file, "%d", Dkeyframes[i].getIDPtr());
        fscanf(file, "%f", Dkeyframes[i].getTimePtr());
        
        for( int j = 0; j < Keyframe::NUM_JOINT_ENUM; j++ )
            fscanf(file, "%f", Dkeyframes[i].getDOFPtr(j));
    }
    fclose(file);

}


// Initialize glut and create a window with the specified caption
void initGlut(int argc, char** argv)
{
	// Init GLUT
	glutInit(&argc, argv);

    // Set video mode: double-buffered, color, depth-buffered
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

    // Create window
    glutInitWindowPosition (0, 0);
    glutInitWindowSize(Win[0],Win[1]);
    windowID = glutCreateWindow(argv[0]);

    // Setup callback functions to handle events
    glutReshapeFunc(reshape);	// Call reshape whenever window resized
    glutDisplayFunc(display);	// Call display whenever new frame needed
	glutMouseFunc(mouse);		// Call mouse whenever mouse button pressed
	glutMotionFunc(motion);		// Call motion whenever mouse moves while button pressed
    glutKeyboardFunc(keyboard); // Call keyboard whenever keyboard press

}



// Quit button handler.  Called when the "quit" button is pressed.
void quitButton(int)
{
  exit(0);
}

// Initialize GLUI and the user interface
void initGlui()
{
	GLUI_Panel* glui_panel;
	GLUI_Spinner* glui_spinner;
	GLUI_RadioGroup* glui_radio_group;

    GLUI_Master.set_glutIdleFunc(NULL);


	// Create GLUI window (joint controls) ***************
	//
	glui_joints = GLUI_Master.create_glui("Joint Control", 0, Win[0]+12, 0);


	///////////////////////////////////////////////////////////
	// TODO (for controlling light source position & additional
	//      rendering styles):
	//   Add more UI spinner elements here. Be sure to also
	//   add the appropriate min/max range values to this
	//   file, and to also add the appropriate enums to the
	//   enumeration in the Keyframe class (keyframe.h).
	///////////////////////////////////////////////////////////
    glui_panel = glui_joints->add_panel("Light controll");
    glui_spinner = glui_joints->add_spinner_to_panel(glui_panel, "Light", GLUI_SPINNER_FLOAT, &light_pos);
    glui_spinner->set_float_limits(0, 360, GLUI_LIMIT_CLAMP);
    glui_spinner->set_speed(SPINNER_SPEED);
	//
	// ***************************************************


	// Add button to quit

	// ***************************************************


	// Create GLUI window (render controls) ************
	//
	glui_render = GLUI_Master.create_glui("Render Control", 0, 367, Win[1]+64);

	// Create control to specify the render style
	glui_panel = glui_render->add_panel("Render Style");
	glui_radio_group = glui_render->add_radiogroup_to_panel(glui_panel, &renderStyle);
	glui_render->add_radiobutton_to_group(glui_radio_group, "Wireframe");
	glui_render->add_radiobutton_to_group(glui_radio_group, "Solid");
	glui_render->add_radiobutton_to_group(glui_radio_group, "Solid w/ outlines");
    glui_render->add_radiobutton_to_group(glui_radio_group, "Mettalic");
    glui_render->add_radiobutton_to_group(glui_radio_group, "Matte");
    
    glui_panel = glui_render->add_panel("Difficulty");
    glui_radio_group = glui_render->add_radiogroup_to_panel(glui_panel, &difficulty);
    glui_render->add_radiobutton_to_group(glui_radio_group, "Hard");
    glui_render->add_radiobutton_to_group(glui_radio_group, "Insane");
    glui_render->add_radiobutton_to_group(glui_radio_group, "Lunatic");
    
    glui_panel = glui_render->add_panel("", GLUI_PANEL_NONE);
    glui_render->add_button_to_panel(glui_panel, "Quit", 0, quitButton);
    //
	//
	// ***************************************************


	// Tell GLUI windows which window is main graphics window
	glui_joints->set_main_gfx_window(windowID);
	glui_render->set_main_gfx_window(windowID);
}


// Performs most of the OpenGL intialization
void initGl(void)
{
    // glClearColor (red, green, blue, alpha)
    // Ignore the meaning of the 'alpha' value for now
    glClearColor(230/255.0, 242/255.0, 255/255.0,1.0f);
    glEnable(GL_DEPTH_TEST);

    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    GLfloat position[] = {0, 0, 15, 1.0f};
    glLightfv(GL_LIGHT0, GL_POSITION, position);
    GLUI_Master.set_glutIdleFunc(animate);
    
}

//Cubic interpolation
Vector getAnimatonInterpolatedJointDOFS(float time)
{
    // Need to find the keyframes bewteen which
    // the supplied time lies.
    // At the end of the loop we have:
    //    keyframes[i-1].getTime() < time <= keyframes[i].getTime()
    //
    Keyframe * curKeyframe = NULL;
    int validkeyframe = 0;
    switch (curAnimationMode) {
        case 1:curKeyframe = Wkeyframes;
            validkeyframe = maxValidWKeyframe;
            break;
        case 2:curKeyframe = Skeyframes;
            validkeyframe = maxValidSKeyframe;
            break;
        case 3:curKeyframe = Akeyframes;
            validkeyframe = maxValidAKeyframe;
            break;
        case 4:curKeyframe = Dkeyframes;
            validkeyframe = maxValidDKeyframe;
            break;
    }
    int i = 0;
    while( i <= validkeyframe && curKeyframe[i].getTime() < time )
        i++;
    
    // If time is before or at first defined keyframe, then
    // just use first keyframe pose
    if( i == 0 )
        return curKeyframe[0].getDOFVector();
    
    // If time is beyond last defined keyframe, then just
    // use last keyframe pose
    if( i > validkeyframe )
        return curKeyframe[validkeyframe].getDOFVector();
    
    // Need to normalize time to (0, 1]

    
    // Get appropriate data points and tangent vectors
//    // for computing the interpolation
    int start_index = 1;
    int end_index = 2;
    Vector p[4];
    float keyframetime[4];
    float starttime;
    float endtime;
    Vector return_vector(Keyframe::NUM_JOINT_ENUM);
    p[1] = curKeyframe[i-1].getDOFVector();
    keyframetime[1] =curKeyframe[i-1].getTime();
    starttime = keyframetime[1];
    p[2] = curKeyframe[i].getDOFVector();
    keyframetime[2] =curKeyframe[i].getTime();
    endtime = keyframetime[2];
    if((i+1)<=validkeyframe){
        p[3] = curKeyframe[i+1].getDOFVector();
        keyframetime[3] =curKeyframe[i+1].getTime();
        end_index++ ;
        endtime = keyframetime[3];
    }
    if ((i-1)>0){
        p[0] = curKeyframe[i-1].getDOFVector();
        keyframetime[0] =curKeyframe[i-2].getTime();
        start_index--;
        starttime = keyframetime[0];
    }
    for (int n=start_index;n<end_index+1;n++){
        keyframetime[n] = timenormal(keyframetime[n],starttime,endtime);
    }
    //Only two keyframe, use linear interpolation
    if((end_index-start_index+1) == 2){
        time = (time - curKeyframe[i-1].getTime()) / (curKeyframe[i].getTime() - curKeyframe[i-1].getTime());
        return p[1] + (p[2]-p[1])*time;

    }
    //Only 3 keyframe, need add one derivative term
    else if( (end_index-start_index+1) == 3){
        time = timenormal(time, starttime, endtime);
        for(int m=0;m<p[1].getDim();m++){
            CubicInterpolation * cubic = new CubicInterpolation();
            int k = 0;
            for (int j=start_index;j<end_index+1;j++){
                cubic->addEquation(keyframetime[j],p[j].getData()[m], false, k);
                k++;
            }
            cubic->addEquation(keyframetime[start_index], (p[start_index]-p[start_index+1]).getData()[m]/2, true, 3);
            cubic->solve();
            return_vector.setData(cubic->calculate(time), m);
        }
        return return_vector;
    }
    //4 keyframes, good
    else{
        time = timenormal(time, starttime, endtime);
        for(int m=0;m<p[1].getDim();m++){
            CubicInterpolation * cubic = new CubicInterpolation();
            for (int j=start_index;j<end_index+1;j++){
                cubic->addEquation(keyframetime[j],p[j].getData()[m], false, j);
            }
            cubic->solve();
            return_vector.setData(cubic->calculate(time), m);
        }
        return return_vector;
    }
}



void restart(){
    platform_length = 60;
    for(int i=0;i<curEnemy;i++){
        enemies[i].deploy(platform_length);
    }
    animationTimer->reset();
}

// Callback idle function for animating the scene
void animate()
{
	// Only update if enough time has passed
	// (This locks the display to a certain frame rate rather
	//  than updating as fast as possible. The effect is that
	//  the animation should run at about the same rate
	//  whether being run on a fast machine or slow machine)
	if( frameRateTimer->elapsed() > SEC_PER_FRAME )
	{
		// Tell glut window to update itself. This will cause the display()
		// callback to be called, which renders the object (once you've written
		// the callback).
		glutSetWindow(windowID);
		glutPostRedisplay();
        //Animation


		// Restart the frame rate timer
		// for the next frame
		frameRateTimer->reset();
	}

}

// Handles the window being resized by updating the viewport
// and projection matrices
void reshape(int w, int h)
{
	// Update internal variables and OpenGL viewport
	Win[0] = w;
	Win[1] = h;
	glViewport(0, 0, (GLsizei)Win[0], (GLsizei)Win[1]);

    // Setup projection matrix for new window
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	gluPerspective(CAMERA_FOVY, (GLdouble)Win[0]/(GLdouble)Win[1], NEAR_CLIP, FAR_CLIP);
}



// display callback
//
// README: This gets called by the event handler
// to draw the scene, so this is where you need
// to build your scene -- make your changes and
// additions here. All rendering happens in this
// function. For Assignment 2, updates to the
// joint DOFs (joint_ui_data) happen in the
// animate() function.
void display(void)
{
           // Clear the screen with the background colour
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  

    // Setup the model-view transformation matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    if(prev_difficulty != difficulty){
        prev_difficulty = difficulty;
        switch (difficulty) {
            case 0:
                curEnemy = 10;
                break;
            case 1:
                curEnemy = 20;
                break;
            case 2:
                curEnemy = 30;
                break;
        }
        restart();
    }

    if( animate_mode )
    {
        
        enemy_rotate = int(enemy_rotate+20)%360;
        for(int i=0;i<curEnemy;i++){
            enemies[i].move();
            
            if (fabs(enemies[i].getX()-joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_X))<1.5 &&
                fabs(enemies[i].getY()-joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_Y))<1.5){
                animate_mode = 0;
                animationTimer->reset();
                GLUI_Master.set_glutIdleFunc(NULL);
                keypress = 0;
                restart();
            }
            if (enemies[i].outofplatform(platform_length,platform_buffer)){
                enemies[i].deploy(platform_length);
            }
        }
        float curTime = animationTimer->elapsed();
        Keyframe * curkeyframe = NULL;
        int curMaxValid = 0;
        switch (curAnimationMode) {
            case 1:curkeyframe = Wkeyframes;
                curMaxValid = maxValidWKeyframe;
                break;
            case 2:curkeyframe = Skeyframes;
                curMaxValid = maxValidSKeyframe;
                break;
            case 3:curkeyframe = Akeyframes;
                curMaxValid = maxValidAKeyframe;
                break;
            case 4:curkeyframe = Dkeyframes;
                curMaxValid = maxValidDKeyframe;
                break;
            default:
                curkeyframe = NULL;
                break;
        }
        if (curkeyframe){
            if( curTime >= curkeyframe[curMaxValid].getTime() )
            {
                printf("$");
                // Stop the animation
                Wkeyframes[0].setDOF(Keyframe::ROOT_TRANSLATE_X, Wkeyframes[maxValidWKeyframe].getDOF(Keyframe::ROOT_TRANSLATE_X));
                Skeyframes[0].setDOF(Keyframe::ROOT_TRANSLATE_X, Skeyframes[maxValidSKeyframe].getDOF(Keyframe::ROOT_TRANSLATE_X));
                Akeyframes[0].setDOF(Keyframe::ROOT_TRANSLATE_X, Akeyframes[maxValidAKeyframe].getDOF(Keyframe::ROOT_TRANSLATE_X));
                Dkeyframes[0].setDOF(Keyframe::ROOT_TRANSLATE_X, Dkeyframes[maxValidDKeyframe].getDOF(Keyframe::ROOT_TRANSLATE_X));
                
                Wkeyframes[0].setDOF(Keyframe::ROOT_TRANSLATE_Y, Wkeyframes[maxValidWKeyframe].getDOF(Keyframe::ROOT_TRANSLATE_Y));
                Skeyframes[0].setDOF(Keyframe::ROOT_TRANSLATE_Y, Skeyframes[maxValidSKeyframe].getDOF(Keyframe::ROOT_TRANSLATE_Y));
                Akeyframes[0].setDOF(Keyframe::ROOT_TRANSLATE_Y, Akeyframes[maxValidAKeyframe].getDOF(Keyframe::ROOT_TRANSLATE_Y));
                Dkeyframes[0].setDOF(Keyframe::ROOT_TRANSLATE_Y, Dkeyframes[maxValidDKeyframe].getDOF(Keyframe::ROOT_TRANSLATE_Y));
                joint_ui_data->setDOFVector(curkeyframe[0].getDOFVector());
                curAnimationMode = 0;
                keypress = 0;
                animationTimer->reset();
            }
            else{
                joint_ui_data->setDOFVector( getAnimatonInterpolatedJointDOFS(curTime) );
                // Update user interface
                joint_ui_data->setTime(curTime);
            }
        }
    }
    
    
    

	// Specify camera transformation
    camXPos = joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_X) + 15 * cos(camRotate);
    camYPos = joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_Y) + 15 * sin(camRotate);
    gluLookAt(camXPos, camYPos, camZPos, joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_X), joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_Y), joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_Z), 0, 0, 1);
	glPushMatrix();
		// setup transformation for body part
    float light_x = 2*cos(light_pos/90.0*PI/2);
    float light_y =  2*sin(light_pos/90.0*PI/2);
    GLfloat position[] = {light_x,light_y, 5, 1.0f};
//    GLfloat diffuseRGBA[] = {0.5, 0.5, 0.5, 1.0f};
//    GLfloat specularRGBA[] = {0.5, 0.5, 0.5, 1.0f};
//    GLfloat ambientRGBA[] = {0,0,0};
//    glMaterialfv(GL_FRONT, GL_AMBIENT, ambientRGBA);
//    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseRGBA);
//    glMaterialfv(GL_FRONT, GL_SPECULAR, specularRGBA);
//    glMaterialf(GL_FRONT, GL_SHININESS, 0 );
    glLightfv(GL_LIGHT0, GL_POSITION, position);
		// determine render style and set glPolygonMode appropriately
    if (renderStyle == WIREFRAME){
        glDisable(GL_LIGHTING);
        glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
        glColor3f(1.0, 1.0, 1.0);
        drawMain();
    }
    else if (renderStyle == SOLID){
        glDisable(GL_LIGHTING);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glColor3f(1.0, 1.0, 1.0);
        drawMain();
    }
    else if(renderStyle == OUTLINED){
        glDisable(GL_LIGHTING);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glEnable(GL_POLYGON_OFFSET_FILL);
        glPolygonOffset(1,1);
        glColor3f(1, 1, 1);
        drawMain();
        glDisable(GL_POLYGON_OFFSET_FILL);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glColor3f(0, 0, 0);
        drawOutline();
    }
    else if (renderStyle == METALLIC){
        glEnable(GL_LIGHTING);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glEnable(GL_LIGHT1);
        float light2_x = (camXPos +joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_X))/2.0;
        float light2_y = (camYPos +joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_Y))/2.0;
        float light2_z = (camZPos +joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_Z))/2.0;
        GLfloat light2position[] = {light2_x, light2_y, light2_z, 1.0f};
        glLightfv(GL_LIGHT0, GL_POSITION, light2position);
        drawMain();
    }
    else if (renderStyle == MATTE){
        glEnable(GL_LIGHTING);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        GLfloat specularRGBA[] = {0.4,0.4,0.4, 1.0f};
        GLfloat diffuseRGBA[] = {0.04,0.04,0.04, 1.0f};
        GLfloat ambientRGBA[] = {0.4,0.05,0.05};
        glMaterialfv(GL_FRONT, GL_AMBIENT, ambientRGBA);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseRGBA);
        glMaterialfv(GL_FRONT, GL_SPECULAR, specularRGBA);
        glMaterialf(GL_FRONT, GL_SHININESS, 0 );
        drawMain();
        
        
    }
	// SAMPLE CODE **********

    // Execute any GL functions that are in the queue just to be safe
    glFlush();
    // Now, show the frame buffer that we just drew into.
    // (this prevents flickering).
    glutSwapBuffers();
}


// Handles mouse button pressed / released events
void mouse(int button, int state, int x, int y)
{
	// If the RMB is pressed and dragged then zoom in / out
	if( button == GLUT_RIGHT_BUTTON )
	{
		if( state == GLUT_DOWN )
		{
			lastX = x;
			lastY = y;
			updateCamZPos = true;
		}
		else
		{
			updateCamZPos = false;
		}
	}
    if( button == GLUT_LEFT_BUTTON )
    {
        if( state == GLUT_DOWN )
        {
            lastX = x;
            lastY = y;
            updateCamZPos = true;
        }
        else
        {
            updateCamZPos = false;
        }
    }
}
int updatePenguinPos(float oldvalue, float increment, int type){
    if (!((oldvalue + increment) < platform_length/2.0 && (oldvalue+increment) > -platform_length/2.0)){
        return 0;
    }
    if (type == 0){
        if (increment>0){
            for(int j =1;j<maxValidWKeyframe+1;j++){
                Wkeyframes[j].setDOF(Keyframe::ROOT_TRANSLATE_X, oldvalue+increment* j * 1.0/maxValidWKeyframe);
            }
            for(int j =1;j<maxValidSKeyframe+1;j++){
                Skeyframes[j].setDOF(Keyframe::ROOT_TRANSLATE_X, oldvalue+increment);
            }
        }
        else if(increment<0){
             for(int j =1;j<maxValidSKeyframe+1;j++){
            Skeyframes[j].setDOF(Keyframe::ROOT_TRANSLATE_X, oldvalue+increment* j * 1.0/maxValidSKeyframe);
             }
            for(int j =1;j<maxValidWKeyframe+1;j++){
                Wkeyframes[j].setDOF(Keyframe::ROOT_TRANSLATE_X, oldvalue+increment);
            }
        }
        for(int j =0;j<maxValidAKeyframe+1;j++){
            Akeyframes[j].setDOF(Keyframe::ROOT_TRANSLATE_X, oldvalue+increment);
            printf("Akeyframe%d:%f\n", j, Akeyframes[j].getDOF(Keyframe::ROOT_TRANSLATE_X));
        }
        for(int j =0;j<maxValidDKeyframe+1;j++){
            Dkeyframes[j].setDOF(Keyframe::ROOT_TRANSLATE_X, oldvalue+increment);
        }
    }
    else{
        if (increment>0){
            for(int j =1;j<maxValidAKeyframe+1;j++){
                Akeyframes[j].setDOF(Keyframe::ROOT_TRANSLATE_Y, oldvalue+increment*j * 1.0/maxValidAKeyframe);
            }
            for(int j =1;j<maxValidDKeyframe+1;j++){
                Dkeyframes[j].setDOF(Keyframe::ROOT_TRANSLATE_Y, oldvalue+increment);
            }
        }
        else if(increment<0){
            for(int j =1;j<maxValidDKeyframe+1;j++){
                Dkeyframes[j].setDOF(Keyframe::ROOT_TRANSLATE_Y, oldvalue+increment*j * 1.0/maxValidDKeyframe);
            }
            for(int j =1;j<maxValidAKeyframe+1;j++){
                Akeyframes[j].setDOF(Keyframe::ROOT_TRANSLATE_Y, oldvalue+increment);
            }
        }
        for(int j =0;j<maxValidWKeyframe+1;j++){
            Wkeyframes[j].setDOF(Keyframe::ROOT_TRANSLATE_Y, oldvalue+increment);
        }
        for(int j =0;j<maxValidSKeyframe+1;j++){
            Skeyframes[j].setDOF(Keyframe::ROOT_TRANSLATE_Y, oldvalue+increment);
        }
    }
    return 1;
}

// Handles mouse motion events while a button is pressed
void motion(int x, int y)
{
	// If the RMB is pressed and dragged then zoom in / out
	if( updateCamZPos )
	{
		// Update camera z position
//		camZPos += (x - lastX) * ZOOM_SCALE;
//		lastX = x;
        int change = x - lastX;
        float theta = atan2(change/10.0, 10);
        camRotate+= theta;
        camXPos = joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_X) + 10 * cos(camRotate);
        camYPos = joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_Y) + 10 * sin(camRotate);
        lastX = x;
		// Redraw the scene from updated camera position
		glutSetWindow(windowID);
        glutPostRedisplay();

		
	}
}

//Handles void keyboard(unsigned char Key, int x, int y);
void keyboard(unsigned char Key, int x, int y){
    float old_x =joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_X);
    float old_y = joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_Y);
    printf("%c, %d\n",Key , keypress);
    if (!keypress){
    switch(Key){
        case 'w':if(updatePenguinPos(old_x,steplength,0) && animate_mode){
            curAnimationMode = 1;
            animationTimer->reset();
            keypress = 1;
        }
            break;
        case 's':if(updatePenguinPos(old_x,-steplength,0) && animate_mode){
            curAnimationMode = 2;
            animationTimer->reset();
            keypress = 1;
        }
            break;
        case 'a':if(updatePenguinPos(old_y,steplength,1) && animate_mode){
            curAnimationMode = 3;
            animationTimer->reset();
            keypress =1;
        }
            break;
        case 'd':if(updatePenguinPos(old_y,-steplength,1) && animate_mode){
            curAnimationMode = 4;
            animationTimer->reset();
            keypress = 1;
        }
            break;
        case ' ':
            animate_mode = 1;
            keypress = 0;
            printf("restart\n");
            GLUI_Master.set_glutIdleFunc(animate);break;
    
    }
    }
    glutPostRedisplay();


}


// Draw a unit cube, centered at the current location
// README: Helper code for drawing a cube
void drawCube()
{
	glBegin(GL_QUADS);
		// draw front face
        glNormal3d(0, 0, 1);
		glVertex3f(-1.0, -1.0, 1.0);
		glVertex3f( 1.0, -1.0, 1.0);
		glVertex3f( 1.0,  1.0, 1.0);
		glVertex3f(-1.0,  1.0, 1.0);
    
		// draw back face
        glNormal3d(0, 0, -1);
		glVertex3f( 1.0, -1.0, -1.0);
		glVertex3f(-1.0, -1.0, -1.0);
		glVertex3f(-1.0,  1.0, -1.0);
		glVertex3f( 1.0,  1.0, -1.0);

		// draw left face
        glNormal3d(-1, 0, 0);
		glVertex3f(-1.0, -1.0, -1.0);
		glVertex3f(-1.0, -1.0,  1.0);
		glVertex3f(-1.0,  1.0,  1.0);
		glVertex3f(-1.0,  1.0, -1.0);

		// draw right face
        glNormal3d(1, 0, 0);
		glVertex3f( 1.0, -1.0,  1.0);
		glVertex3f( 1.0, -1.0, -1.0);
		glVertex3f( 1.0,  1.0, -1.0);
		glVertex3f( 1.0,  1.0,  1.0);

		// draw top
        glNormal3d(0, 1, 0);
		glVertex3f(-1.0,  1.0,  1.0);
		glVertex3f( 1.0,  1.0,  1.0);
		glVertex3f( 1.0,  1.0, -1.0);
		glVertex3f(-1.0,  1.0, -1.0);

		// draw bottom
        glNormal3d(0, -1, 0);
		glVertex3f(-1.0, -1.0, -1.0);
		glVertex3f( 1.0, -1.0, -1.0);
		glVertex3f( 1.0, -1.0,  1.0);
		glVertex3f(-1.0, -1.0,  1.0);
	glEnd();
}
void drawBody(int color)
{
    glScaled(1, 1.0, 1.5);
    glBegin(GL_QUADS);
    if (color){
            glColor3f(38/255.0, 38/255.0, 38/255.0);
    }
    // draw front face
    glNormal3d(0, 0, 1);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);

    // draw back face
    glNormal3d(0, 0, -1);
    glVertex3f( 1.5, -2.0, -2.0);
    glVertex3f(-1.5, -2.0, -2.0);
    glVertex3f(-1.5,  2.0, -2.0);
    glVertex3f( 1.5,  2, -2.0);
    
    // draw left face
    glNormal3d(-6.0/sqrt(37), 0, 1.0/sqrt(37));
    glVertex3f(-1.5, -2.0, -2.0);
    glVertex3f(-1.0, -1.0,  1.0);
    glVertex3f(-1.0,  1.0,  1.0);
    glVertex3f(-1.5,  2.0, -2.0);
    if (color){
                glColor3f(1, 1, 1);
    }
    // draw right face
    glNormal3d(6.0/sqrt(37), 0, 1.0/sqrt(37));
    glVertex3f( 1.0, -1.0,  1.0);
    glVertex3f( 1.5, -2.0, -2.0);
    glVertex3f( 1.5,  2.0, -2.0);
    glVertex3f( 1.0,  1.0,  1.0);
    if (color){
         glColor3f(38/255.0, 38/255.0, 38/255.0);
    }
    // draw top
    glNormal3d(0, 3.0/sqrt(10), 1.0/sqrt(10));
    glVertex3f(-1.0,  1.0,  1.0);
    glVertex3f( 1.0,  1.0,  1.0);
    glVertex3f( 1.5,  2.0, -2.0);
    glVertex3f(-1.5,  2.0, -2.0);
    
    // draw bottom
    glNormal3d(0, -3.0/sqrt(10), 1.0/sqrt(10));
    glVertex3f(-1.5, -2, -2.0);
    glVertex3f( 1.5, -2, -2.0);
    glVertex3f( 1.0, -1.0,  1.0);
    glVertex3f(-1.0, -1.0,  1.0);
    glEnd();
}
void drawWing()
{
    glScaled(1, 1, 1);
    glBegin(GL_QUADS);
    // draw front face
    glNormal3d(0, 0, 1);
    glVertex3f(-1, -0.25, 1.0);
    glVertex3f( 1, -0.25, 1.0);
    glVertex3f( 1,  0.25, 1.0);
    glVertex3f(-1,  0.25, 1.0);
    
    // draw back face
    glNormal3d(0, 0, -1);
    glVertex3f( 0.5, -0.25, -1.0);
    glVertex3f(-1, -0.25, -1.0);
    glVertex3f(-1,  0.25, -1.0);
    glVertex3f( 0.5,  0.25, -1.0);
    
    // draw left face
    glNormal3d(0, 0, -1);
    glVertex3f(-1, 0.25, -1.0);
    glVertex3f(-1,  0.25,  1.0);
    glVertex3f(-1, -0.25,  1.0);
    glVertex3f(-1,  -0.25, -1.0);
    
    // draw right face
    glNormal3d(4.0/sqrt(17), 0, -1.0/sqrt(17));
    glVertex3f( 0.5, -0.25, -1.0);
    glVertex3f( 0.5, 0.25, -1.0);
    glVertex3f( 1,  0.25,  1.0);
    glVertex3f( 1, -0.25,  1.0);
    
    
    // draw top
    glNormal3d(0, 1, 0);
    glVertex3f(-1,  0.25,  1.0);
    glVertex3f( 1,  0.25,  1.0);
    glVertex3f( 0.5,  0.25, -1.0);
    glVertex3f(-1,  0.25, -1.0);
    
    // draw bottom
    glNormal3d(0, -1, 0);
    glVertex3f(-1, -0.25, -1.0);
    glVertex3f( 0.5, -0.25, -1.0);
    glVertex3f( 1, -0.25,  1.0);
    glVertex3f(-1, -0.25,  1.0);
    glEnd();
}
void drawFeet(){
    glBegin(GL_QUADS);
    //Top
    glNormal3d(0, 0, 1);
    glVertex3f(0, 0, 0.125);
    glVertex3f( 0.5, 0.25, 0.125);
    glVertex3f( 0.5,  -0.25, 0.125);
    glVertex3f(0, 0, 0.125);
    //Bottom
    glNormal3d(0, 0, -1);
    glVertex3f(0, 0, -0.125);
    glVertex3f( 0.5,  0.25, -0.125);
    glVertex3f( 0.5, -0.25, -0.125);
    glVertex3f(0, 0, -0.125);
    
    //Up
    glNormal3d(-1.0/sqrt(5), 2.0/sqrt(5), 0);
    glVertex3f(0, 0, -0.125);
    glVertex3f( 0.5, 0.25, -0.125);
     glVertex3f( 0.5,  0.25, 0.125);
    glVertex3f( 0,  0, 0.125);
    //Down
    glNormal3d(-1.0/sqrt(5), -2.0/sqrt(5), 0);
    glVertex3f( 0,  0, 0.125);
    glVertex3f(0, 0, -0.125);
    glVertex3f( 0.5,  -0.25, -0.125);
    glVertex3f( 0.5, -0.25, 0.125);
    //Right
    glNormal3d(1, 0, 0);
    glVertex3f(0.5, 0.25, 0.125);
    glVertex3f( 0.5, 0.25, -0.125);
    glVertex3f( 0.5,  -0.25, -0.125);
    glVertex3f( 0.5,  -0.25, 0.125);
    glEnd();
}
void drawHead(int color){
    glPushMatrix();
    glTranslatef(0, 0, 1.5);
    if (color){
        glColor3f(222/255.0, 167/255.0, 58/255.0);
    }
    glRotatef(joint_ui_data->getDOF(Keyframe::HEAD), 0, 0, 1);
        glPushMatrix();
        glTranslatef(2.4, 0, -0.4 - 0.2*joint_ui_data->getDOF(Keyframe::BEAK));
        glScaled(1.2, 0.4, 0.2);
        drawCube();
        glPopMatrix();
        glPushMatrix();
        glTranslatef(2.4, 0, 0);
        glScaled(1.2, 0.4, 0.2);
        drawCube();
        glPopMatrix();
    if (color){
        glColor3f(38/255.0 , 38/255.0  , 38/255.0);
    }
    glScaled(1.2, 0.5, 0.5);
    drawCube();
    glPopMatrix();
}
void drawWings(){
    glPushMatrix();
    drawWing();
    glPopMatrix();
}
void drawMain(){

   GLfloat diffuseRGBA[] = {1, 1, 1, 1.0f};
   GLfloat specularRGBA[] = {1, 1, 1, 1.0f};
   GLfloat ambientRGBA[] = {1,1,1};
   glMaterialfv(GL_FRONT, GL_AMBIENT, ambientRGBA);
   glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseRGBA);
   glMaterialfv(GL_FRONT, GL_SPECULAR, specularRGBA);
   glMaterialf(GL_FRONT, GL_SHININESS, 0.4f*128.0 );
    //Platform
    glColor3f(1, 1, 1);
    glPushMatrix();
    glTranslatef(0.0, 0.0, -11.5);
    glScaled(platform_length/2.0, platform_length/2.0, 5);
    drawCube();
    glPopMatrix();
    
    for (int i=0; i<curEnemy;i++){
        int direction = enemies[i].getDirection()/2;
        float alpha = 0;
        float beta = 0;
        if(direction == 1){
            alpha = 1.0 - fabs(enemies[i].getY() - platform_length/2.0)/10;
            beta =  1.0 - fabs(enemies[i].getY() + platform_length/2.0)/10;
            drawCircle(alpha * 2,100,enemies[i].getX(),platform_length/2, direction);
            drawCircle(beta * 2,100,enemies[i].getX(),-platform_length/2 ,direction);
        }
        else{
            alpha = 1.0 - fabs(enemies[i].getX() - platform_length/2.0)/10;
            beta =  1.0 - fabs(enemies[i].getX() + platform_length/2.0)/10;
            drawCircle(alpha * 2,100,platform_length/2,enemies[i].getY() ,direction);
            drawCircle(beta * 2,100,-platform_length/2,enemies[i].getY() ,direction);
        }
        if(enemies[i].intoplatform(platform_length)){
        glPushMatrix();
       GLfloat diffuseRGBA[] = {0.75164, 0.60648, 0.22648, 1.0f};
       GLfloat specularRGBA[] = {0.628281, 0.555802, 0.366065, 1.0f};
       GLfloat ambientRGBA[] = {0.24725,0.1995,0.0745};
       glMaterialfv(GL_FRONT, GL_AMBIENT, ambientRGBA);
       glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseRGBA);
       glMaterialfv(GL_FRONT, GL_SPECULAR, specularRGBA);
       glMaterialf(GL_FRONT, GL_SHININESS, 0.4f*128.0 );
        int * tempcolor =enemies[i].GetColor();
        glColor3f(tempcolor[0]/255.0, tempcolor[1]/255.0, tempcolor[2]/255.0);
        glTranslatef(enemies[i].getX(), enemies[i].getY(), 0);
        glRotated((int(enemy_rotate))%360, direction, 1-direction, 0);
        drawCube();
        glPopMatrix();
        }
    }
    
    glTranslatef(joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_X),
                 joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_Y),
                 joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_Z));
    glRotatef(joint_ui_data->getDOF(Keyframe::ROOT_ROTATE_X),1.0, 0.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::ROOT_ROTATE_Y),0.0, 1.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::ROOT_ROTATE_Z),0.0, 0.0, 1.0);
    
    glColor3f(1.0, 1.0, 1.0);
    drawBody(1);

    drawHead(1);
    glColor3f(75/255.0, 75/255.0, 75/255.0);
    //Draw Right shoulder
    glPushMatrix();
    glTranslatef(0, -1.2, 0);
    glRotatef(-20-joint_ui_data->getDOF(Keyframe::R_SHOULDER_PITCH),1.0, 0.0, 0.0);
    glRotatef(-joint_ui_data->getDOF(Keyframe::R_SHOULDER_YAW),0.0, 1.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::R_SHOULDER_ROLL),0.0, 0.0, 1.0);
    glScaled(1, 1, joint_ui_data->getDOF(Keyframe::R_ARM_SCALE));
    glPushMatrix();
    glTranslatef(0, 0, -1);
    drawWings();
    glPopMatrix();
    //Draw Right arm
    glPushMatrix();
    glTranslatef(0, 0, -2);
    glRotated(-joint_ui_data->getDOF(Keyframe::R_ELBOW), 0, 1, 0);
    glTranslatef(0, 0, -0.5);
    glScaled(0.5, 0.2, 0.5);
    drawCube();
    glPopMatrix();
    glPopMatrix();
    //Draw Left shoulder
    glPushMatrix();
    glTranslatef(0, 1.2, 0);
    glRotatef(20+joint_ui_data->getDOF(Keyframe::L_SHOULDER_PITCH),1.0, 0.0, 0.0);
    glRotatef(-joint_ui_data->getDOF(Keyframe::L_SHOULDER_YAW),0.0, 1.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::L_SHOULDER_ROLL),0.0, 0.0, 1.0);
    glScaled(1, 1, joint_ui_data->getDOF(Keyframe::L_ARM_SCALE));
    glPushMatrix();
    glTranslatef(0, 0, -1);
    drawWings();
    glPopMatrix();
    //Draw Left arm
    glPushMatrix();
    glTranslatef(0, 0, -2);
    glRotated(-joint_ui_data->getDOF(Keyframe::L_ELBOW), 0, 1, 0);
    glTranslatef(0, 0, -0.5);
    glScaled(0.5, 0.2,0.5);
    drawCube();
    glPopMatrix();
    glPopMatrix();
    glColor3f(183/255.0, 74/255.0, 32/255.0);
    //Draw Right leg
    glPushMatrix();
    glTranslatef(0, 0.5, -2);
    glRotatef(joint_ui_data->getDOF(Keyframe::R_HIP_PITCH),1.0, 0.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::R_HIP_YAW),0.0, 1.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::R_HIP_ROLL),0.0, 0.0, 1.0);
    glPushMatrix();
    glTranslatef(0, 0, -1);
    glScaled(0.1, 0.1, 1);
    drawCube();
    glPopMatrix();
    //Draw Left Feet
    glPushMatrix();
    glTranslatef(0, 0, -2);
    glRotated(joint_ui_data->getDOF(Keyframe::R_KNEE), 0, 1, 0);
    drawFeet();
    glPopMatrix();
    glPopMatrix();
    //Draw leg
    glPushMatrix();
    glTranslatef(0, -0.5, -2);
    glRotatef(joint_ui_data->getDOF(Keyframe::L_HIP_PITCH),1.0, 0.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::L_HIP_YAW),0.0, 1.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::L_HIP_ROLL),0.0, 0.0, 1.0);
    glPushMatrix();
    glTranslatef(0, 0, -1);
    glScaled(0.1, 0.1, 1);
    drawCube();
    glPopMatrix();
    //Draw Feet
    glPushMatrix();
    glTranslatef(0, 0, -2);
    glRotated(joint_ui_data->getDOF(Keyframe::L_KNEE), 0, 1, 0);
    drawFeet();
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
}

void drawOutline(){
    //Platform
    glPushMatrix();
    glTranslatef(0.0, 0.0, -11.5);
    glScaled(platform_length/2.0, platform_length/2.0, 5);
    drawCube();
    glPopMatrix();
    
    for (int i=0; i<curEnemy;i++){
        if(enemies[i].intoplatform(platform_length)){
            glPushMatrix();
            glTranslatef(enemies[i].getX(), enemies[i].getY(), 0);
            glRotated((int(enemy_rotate))%360, enemies[i].getDirection()/2, 1-enemies[i].getDirection()/2, 0);
            drawCube();
            glPopMatrix();
        }
    }
    
    glTranslatef(joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_X),
                 joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_Y),
                 joint_ui_data->getDOF(Keyframe::ROOT_TRANSLATE_Z));
    glRotatef(joint_ui_data->getDOF(Keyframe::ROOT_ROTATE_X),1.0, 0.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::ROOT_ROTATE_Y),0.0, 1.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::ROOT_ROTATE_Z),0.0, 0.0, 1.0);
    drawBody(0);
    drawHead(0);
    //Draw Right shoulder
    glPushMatrix();
    glTranslatef(0, -1.2, 0);
    glRotatef(-20-joint_ui_data->getDOF(Keyframe::R_SHOULDER_PITCH),1.0, 0.0, 0.0);
    glRotatef(-joint_ui_data->getDOF(Keyframe::R_SHOULDER_YAW),0.0, 1.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::R_SHOULDER_ROLL),0.0, 0.0, 1.0);
    glScaled(1, 1, joint_ui_data->getDOF(Keyframe::R_ARM_SCALE));
    glPushMatrix();
    glTranslatef(0, 0, -1);
    drawWings();
    glPopMatrix();
    //Draw Right arm
    glPushMatrix();
    glTranslatef(0, 0, -2);
    glRotated(-joint_ui_data->getDOF(Keyframe::R_ELBOW), 0, 1, 0);
    glTranslatef(0, 0, -0.5);
    glScaled(0.5, 0.2, 0.5);
    drawCube();
    glPopMatrix();
    glPopMatrix();
    //Draw Left shoulder
    glPushMatrix();
    glTranslatef(0, 1.2, 0);
    glRotatef(20+joint_ui_data->getDOF(Keyframe::L_SHOULDER_PITCH),1.0, 0.0, 0.0);
    glRotatef(-joint_ui_data->getDOF(Keyframe::L_SHOULDER_YAW),0.0, 1.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::L_SHOULDER_ROLL),0.0, 0.0, 1.0);
    glScaled(1, 1, joint_ui_data->getDOF(Keyframe::L_ARM_SCALE));
    glPushMatrix();
    glTranslatef(0, 0, -1);
    drawWings();
    glPopMatrix();
    //Draw Left arm
    glPushMatrix();
    glTranslatef(0, 0, -2);
    glRotated(-joint_ui_data->getDOF(Keyframe::L_ELBOW), 0, 1, 0);
    glTranslatef(0, 0, -0.5);
    glScaled(0.5, 0.2,0.5);
    drawCube();
    glPopMatrix();
    glPopMatrix();
    //Draw Right leg
    glPushMatrix();
    glTranslatef(0, 0.5, -2);
    glRotatef(joint_ui_data->getDOF(Keyframe::R_HIP_PITCH),1.0, 0.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::R_HIP_YAW),0.0, 1.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::R_HIP_ROLL),0.0, 0.0, 1.0);
    glPushMatrix();
    glTranslatef(0, 0, -1);
    glScaled(0.1, 0.1, 1);
    drawCube();
    glPopMatrix();
    //Draw Left Feet
    glPushMatrix();
    glTranslatef(0, 0, -2);
    glRotated(joint_ui_data->getDOF(Keyframe::R_KNEE), 0, 1, 0);
    drawFeet();
    glPopMatrix();
    glPopMatrix();
    //Draw leg
    glPushMatrix();
    glTranslatef(0, -0.5, -2);
    glRotatef(joint_ui_data->getDOF(Keyframe::L_HIP_PITCH),1.0, 0.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::L_HIP_YAW),0.0, 1.0, 0.0);
    glRotatef(joint_ui_data->getDOF(Keyframe::L_HIP_ROLL),0.0, 0.0, 1.0);
    glPushMatrix();
    glTranslatef(0, 0, -1);
    glScaled(0.1, 0.1, 1);
    drawCube();
    glPopMatrix();
    //Draw Feet
    glPushMatrix();
    glTranslatef(0, 0, -2);
    glRotated(joint_ui_data->getDOF(Keyframe::L_KNEE), 0, 1, 0);
    drawFeet();
    glPopMatrix();
    glPopMatrix();
    //
    
    glPopMatrix();
    //
}

void drawCircle(float r,int n, float x,float y, int direction)
{
    glColor3f(0, 0, 0);
    if (r > 0) {
        glPushMatrix();

        glTranslated(x, y, 0);
        if (direction == 0){
            glRotated(90, 0, 0, 1);
        }
        glNormal3d(0,0,0);
        float theta = 0;
        glBegin(GL_POLYGON);
        for (int i = 0;i<n;i++){
            glVertex3d(r*cos(theta),0,r*sin(theta));
            theta = theta + 2 * M_PI / n;
        }
        glEnd();
        glPopMatrix();
    }
}