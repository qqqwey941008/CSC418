//
//  enemy.h
//  csc411a2
//
//  Created by Kenan Deng on 2016-04-06.
//  Copyright © 2016 Kenan Deng. All rights reserved.
//

#ifndef enemy_h
#define enemy_h
class Enemy
{
public:
    void setX(float i){
        x = i;
    }
    float getX(){
        return x;
    }
    void setY(float i){
        y = i;
    }
    float getY(){
        return y;
    }
    void setId(int i){
        Id = i;
    }
    float getId(){
        return Id;
    }
    void setDest(float x, float y){
        destx = x;
        desty = y;
    }
    float getSpeed(){
        return speed;
    }
    void setSpeed(float s){
        speed = s;
    }
    int getDirection(){
        return direction;
    }
    void move(){
        switch(direction){
            case 0://x-
                x = x-speed;
                break;
            case 1://x+
                x = x+speed;
                break;
            case 2://y-
                y = y-speed;
                break;
            case 3://y+
                y = y + speed;
                break;
                
        }
    }
    void deploy(int platformlength){
        direction = rand()%4;
        int rspeed =rand()%3+1;
        speed = rspeed/10.0;

        switch (rspeed){
            case 3:
                color[0] = 211;
                color[1] = 60;
                color[2] = 55;
                break;
            case 1:
                color[0] = 66;
                color[1] = 146;
                color[2] = 77;
                break;
            case 2:
                color[0] = 247;
                color[1] = 197;
                color[2] = 52;
                break;
        }
        switch (direction){
            case 0:
                x = platformlength + rand()%10;
                y = rand()%platformlength-platformlength/2.0;
                break;
            case 1:
                x = -platformlength - rand()%10;
                y = rand()%platformlength-platformlength/2.0;
                break;
            case 2:
                y = platformlength + rand()%10;
                x = rand()%platformlength-platformlength/2.0;
                break;
            case 3:
                y = -platformlength - rand()%10;
                x = rand()%platformlength-platformlength/2.0;
                break;
        }
    }
    
    int * GetColor(){
        return color;
    }
    
    bool outofplatform(int platformlength, int buffer){
        switch (direction){
            case 0:
                return x  < -platformlength;break;
            case 1:
                return x  > platformlength;break;
            case 2:
                return y  < -platformlength;break;
            case 3:
                return y  > platformlength; break;
        }
        return false;
    }
    bool intoplatform(int platformlength){
        switch (direction){
            case 0:
            case 1:
                return x  > -platformlength/2 && x <platformlength/2;break;
            case 2:
            case 3:
                return y  > -platformlength/2 && y <platformlength/2; break;
        }
        return false;
    }
    
private:
    int    Id;
    float  x;
    float  y;
    float destx;
    float desty;
    float speed;
    int direction;
    int color[3];
};

#endif /* enemy_h */
