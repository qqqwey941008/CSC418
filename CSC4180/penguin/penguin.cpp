/***********************************************************
             CSC418/2504, Fall 2011
  
                 robot.cpp
                 
       Simple demo program using OpenGL and the glut/glui 
       libraries

  
    Instructions:
        Please read the assignment page to determine 
        exactly what needs to be implemented.  Then read 
        over this file and become acquainted with its 
        design.

        Add source code where it appears appropriate. In
        particular, see lines marked 'TODO'.

        You should not need to change the overall structure
        of the program. However it should be clear what
        your changes do, and you should use sufficient comments
        to explain your code.  While the point of the assignment
        is to draw and animate the character, you will
        also be marked based on your design.
		
		Some windows-specific code remains in the includes; 
		we are not maintaining windows build files this term, 
		but we're leaving that here in case you want to try building
		on windows on your own.

***********************************************************/

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <glui.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifndef _WIN32
#include <unistd.h>
#else
void usleep(unsigned int nanosec)
{
    Sleep(nanosec / 1000);
}
#endif


// *************** GLOBAL VARIABLES *************************


const float PI = 3.14159;

// --------------- USER INTERFACE VARIABLES -----------------

// Window settings
int windowID;               // Glut window ID (for display)
GLUI *glui;                 // Glui window (for controls)
int Win[2];                 // window (x,y) size


// ---------------- ANIMATION VARIABLES ---------------------

// Animation settings
int animate_mode = 0;       // 0 = no anim, 1 = animate
int animation_frame = 0;      // Specify current frame of animation

// Joint parameters
const float JOINT_MIN = -45.0f;
const float JOINT_MAX =  45.0f;
float joint_rot = 0.0f;

//////////////////////////////////////////////////////
// TODO: Add additional joint parameters here
//////////////////////////////////////////////////////



// ***********  FUNCTION HEADER DECLARATIONS ****************


// Initialization functions
void initGlut(char* winName);
void initGlui();
void initGl();


// Callbacks for handling events in glut
void myReshape(int w, int h);
void animate();
void display(void);

// Callback for handling events in glui
void GLUI_Control(int id);


// Functions to help draw the object
void drawSquare(float size);
void drawBody(float szie);
void drawWing(float size);
void drawLeg(float size);
void drawFoot(float size);
void drawHead(float size);
void drawMouth(float size);

// Return the current system clock (in seconds)
double getTime();


// ******************** FUNCTIONS ************************



// main() function
// Initializes the user interface (and any user variables)
// then hands over control to the event handler, which calls 
// display() whenever the GL window needs to be redrawn.
int main(int argc, char** argv)
{

    // Process program arguments
    if(argc != 3) {
        printf("Usage: demo [width] [height]\n");
        printf("Using 300x200 window by default...\n");
        Win[0] = 300;
        Win[1] = 200;
    } else {
        Win[0] = atoi(argv[1]);
        Win[1] = atoi(argv[2]);
    }


    // Initialize glut, glui, and opengl
    glutInit(&argc, argv);
    initGlut(argv[0]);
    initGlui();
    initGl();

    // Invoke the standard GLUT main event loop
    glutMainLoop();

    return 0;         // never reached
}


// Initialize glut and create a window with the specified caption 
void initGlut(char* winName)
{
    // Set video mode: double-buffered, color, depth-buffered
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

    // Create window
    glutInitWindowPosition (0, 0);
    glutInitWindowSize(Win[0],Win[1]);
    windowID = glutCreateWindow(winName);

    // Setup callback functions to handle events
    glutReshapeFunc(myReshape); // Call myReshape whenever window resized
    glutDisplayFunc(display);   // Call display whenever new frame needed 
}


// Quit button handler.  Called when the "quit" button is pressed.
void quitButton(int)
{
  exit(0);
}

// Animate button handler.  Called when the "animate" checkbox is pressed.
void animateButton(int)
{
  // synchronize variables that GLUT uses
  glui->sync_live();

  animation_frame = 0;
  if(animate_mode == 1) {
    // start animation
    GLUI_Master.set_glutIdleFunc(animate);
  } else {
    // stop animation
    GLUI_Master.set_glutIdleFunc(NULL);
  }
}

// Initialize GLUI and the user interface
void initGlui()
{
    GLUI_Master.set_glutIdleFunc(NULL);

    // Create GLUI window
    glui = GLUI_Master.create_glui("Glui Window", 0, Win[0]+10, 0);

    // Create a control to specify the rotation of the joint
    GLUI_Spinner *joint_spinner
        = glui->add_spinner("Joint", GLUI_SPINNER_FLOAT, &joint_rot);
    joint_spinner->set_speed(0.1);
    joint_spinner->set_float_limits(JOINT_MIN, JOINT_MAX, GLUI_LIMIT_CLAMP);

    ///////////////////////////////////////////////////////////
    // TODO: 
    //   Add controls for additional joints here
    ///////////////////////////////////////////////////////////

    // Add button to specify animation mode 
    glui->add_separator();
    glui->add_checkbox("Animate", &animate_mode, 0, animateButton);

    // Add "Quit" button
    glui->add_separator();
    glui->add_button("Quit", 0, quitButton);

    // Set the main window to be the "active" window
    glui->set_main_gfx_window(windowID);
}


// Performs most of the OpenGL intialization
void initGl(void)
{
    // glClearColor (red, green, blue, alpha)
    // Ignore the meaning of the 'alpha' value for now
    glClearColor(0.7f,0.7f,0.9f,1.0f);
}




// Callback idle function for animating the scene
void animate()
{
    // Update geometry
    const double joint_rot_speed = 0.1;
    double joint_rot_t = (sin(animation_frame*joint_rot_speed) + 1.0) / 2.0;
    joint_rot = joint_rot_t * JOINT_MIN + (1 - joint_rot_t) * JOINT_MAX;
    
    ///////////////////////////////////////////////////////////
    // TODO:
    //   Modify this function animate the character's joints
    //   Note: Nothing should be drawn in this function!  OpenGL drawing
    //   should only happen in the display() callback.
    ///////////////////////////////////////////////////////////

    // Update user interface
    glui->sync_live();

    // Tell glut window to update itself.  This will cause the display()
    // callback to be called, which renders the object (once you've written
    // the callback).
    glutSetWindow(windowID);
    glutPostRedisplay();

    // increment the frame number.
    animation_frame++;

    // Wait 50 ms between frames (20 frames per second)
    usleep(50000);
}


// Handles the window being resized by updating the viewport
// and projection matrices
void myReshape(int w, int h)
{
    // Setup projection matrix for new window
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-w/2, w/2, -h/2, h/2);

    // Update OpenGL viewport and internal variables
    glViewport(0,0, w,h);
    Win[0] = w;
    Win[1] = h;
}



// display callback
//
// This gets called by the event handler to draw
// the scene, so this is where you need to build
// your scene -- make your changes and additions here.
// All rendering happens in this function.  For Assignment 1,
// updates to geometry should happen in the "animate" function.
void display(void)
{
    // glClearColor (red, green, blue, alpha)
    // Ignore the meaning of the 'alpha' value for now
    glClearColor(0.7f,0.7f,0.9f,1.0f);

    // OK, now clear the screen with the background colour
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  

    // Setup the model-view transformation matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    ///////////////////////////////////////////////////////////
    // TODO:
    //   Modify this function draw the scene
    //   This should include function calls to pieces that
    //   apply the appropriate transformation matrice and
    //   render the individual body parts.
    ///////////////////////////////////////////////////////////

    // Draw our hinged object
    const float BODY_WIDTH = 30.0f;
    const float BODY_LENGTH = 50.0f;
    const float ARM_LENGTH = 50.0f;
    const float ARM_WIDTH = 10.0f;

    // Push the current transformation matrix on the stack
    glPushMatrix();
        
        // Draw the 'body'
        glTranslatef(-50, -280, 0.0);
        glPushMatrix();
            drawBody(130.0);
        glPopMatrix();
        
        // Draw the 'arm'
        glPushMatrix();
            glTranslatef(50.0, 190.0, 0.0);
            drawWing(100.0);
        glPopMatrix();

        // Draw the 'leg&foot'
        // leg: 45 * 150
        // foot: 200 * 40
        glPushMatrix();
            drawLeg(45);
            glTranslatef(130.0, 0.0, 0.0);
            drawLeg(45);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-80.0, -50.0, 0.0);
            drawFoot(200);
            glTranslatef(130.0, 0.0, 0.0);
            drawFoot(200);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-20.0, 500.0, 0.0);
            drawHead(1.0);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-110.0, 520.0, 0.0);
            drawMouth(1.0);
        glPopMatrix();

        // Move the arm to the joint hinge
        glTranslatef(0.0, -BODY_LENGTH/2 + ARM_WIDTH, 0.0);

        // Rotate along the hinge
        glRotatef(joint_rot, 0.0, 0.0, 1.0);

        // Scale the size of the arm
        glScalef(ARM_WIDTH, ARM_LENGTH, 1.0);

        // Move to center location of arm, under previous rotation
        glTranslatef(0.0, -0.5, 0.0);

        // Draw the square for the arm
        glColor3f(1.0, 0.0, 0.0);
        drawSquare(1.0);

    // Retrieve the previous state of the transformation stack
    glPopMatrix();


    // Execute any GL functions that are in the queue just to be safe
    glFlush();

    // Now, show the frame buffer that we just drew into.
    // (this prevents flickering).
    glutSwapBuffers();
}


// Draw a square of the specified size, centered at the current location
void drawSquare(float width)
{
    // Draw the square
    glBegin(GL_LINE_LOOP);
    glVertex2d(-width/2, -width/2);
    glVertex2d(width/2, -width/2);
    glVertex2d(width/2, width/2);
    glVertex2d(-width/2, width/2);
    glEnd();
}

void drawBody(float size){
    glColor3f(0.0, 1.0, 0.0);
    glBegin(GL_LINE_LOOP);
    glVertex2d(0,0);
    glVertex2d(size, 0);
    glVertex2d((1.8)*size, size);
    glVertex2d(size, 4*size);
    glVertex2d(0, 4*size);
    glVertex2d(-0.8*size, size);
    glEnd();
}

void drawWing(float size){
    //
    glColor3f(1.0, 1.0, 0.0);
    glBegin(GL_LINE_LOOP);
    glVertex2d(1.0/6*size,0);
    glVertex2d(5.0/6*size, 0);
    glVertex2d(size, 2*size);
    glVertex2d(0, 2*size);
    glEnd();
}

void drawLeg(float size){
    //45 * 150
    glPushMatrix();
        glScalef(size, size * 150.0/45.0, 0.0);
        drawSquare(1.0);
    glPopMatrix();
}

void drawFoot(float size){
    //200 * 40
    glPushMatrix();
        glScalef(size, size * 40.0/200.0, 0.0);
        drawSquare(1.0);
    glPopMatrix();
}

void drawHead(float size){
    // 0,0 160,0 140,105 60,130 20,105 
    glPushMatrix();
        glScalef(size, size, 0.0);
        glBegin(GL_LINE_LOOP);
        glVertex2d(0.0, 0.0);
        glVertex2d(160.0, 0.0);
        glVertex2d(140.0, 105.0);
        glVertex2d(60.0, 130.0);
        glVertex2d(20.0, 105.0);
        glEnd();
    glPopMatrix();
}

void drawMouth(float size){
    //0,0 110,0 110,10, 0,10
    //0,20 110,20 110,60, 0,35
    glPushMatrix();
        glScalef(size, size, 0.0);
        glBegin(GL_LINE_LOOP);
        glVertex2d(0.0, 0.0);
        glVertex2d(110.0, 0.0);
        glVertex2d(110.0, 10.0);
        glVertex2d(0.0, 10.0);
        glEnd();

        glBegin(GL_LINE_LOOP);
        glVertex2d(0.0, 20.0);
        glVertex2d(110.0, 20.0);
        glVertex2d(110.0, 60.0);
        glVertex2d(0.0, 35.0);
        glEnd();
    glPopMatrix();
}